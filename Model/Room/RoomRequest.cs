﻿namespace AuctionInitService.Model.Room
{
    public class RoomRequest
    {
        public string? Id { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public decimal? EstimatePrice { get; set; }
        public decimal? StepPrice { get; set; }
        public long? Volume { get; set; }
    }
}
