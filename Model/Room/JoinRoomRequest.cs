﻿namespace AuctionInitService.Model.Room
{
    public class JoinRoomRequest
    {
        public string? UserId { get; set; }
        public string? ClientId { get; set; }
        public string[]? ChannelId { get; set; }
    }
}
