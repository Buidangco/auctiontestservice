﻿namespace AuctionInitService.Model.Room
{
    public class HistoryRoomReponse
    {
        public string? UserId { get; set; }
        public decimal Price { get; set; }
        public DateTime Timestamp { get; set; }
        public int Offset { get; set; }
    }
}
