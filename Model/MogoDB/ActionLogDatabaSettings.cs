﻿namespace AuctionInitService.Model.MogoDB
{
    public class ActionLogDatabaSettings
    {
        public string ConnectionString { get; set; } = null!;
        public string DatabaseName { get; set; } = null!;
        public string CollectionName { get; set; } = null!;
    }
}
