﻿namespace AuctionInitService.Model
{
    public class AppSettings
    {
        public string? Secret { get; set; }
        public string? ApiKey { get; set; }
        public bool EnableDelay { get; set; }
        public int TimeDelay { get; set; }
    }
}
