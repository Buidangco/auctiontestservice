﻿namespace AuctionInitService.Model.Channel
{
    public class ChannelRequest
    {
        public string? UserId { get; set; }
        public string? ChannelId { get; set; }
    }
}
