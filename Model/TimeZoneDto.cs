﻿namespace AuctionInitService.Model
{
    public class TimeZoneDto
    {
        public long CurrentTime
        {
            get
            {
                return ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeMilliseconds();
            }
        }
    }
}
