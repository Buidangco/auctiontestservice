﻿namespace AuctionInitService.Model.Kafka
{
    public class AuctionMessage
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public decimal Cost { get; set; }
        public decimal Price { get; set; }

        public string? Winner { get; set; }

        public decimal LastBid { get; set; }
        public AuctionMessage() { } 
    }
}
