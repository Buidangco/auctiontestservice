﻿using Confluent.Kafka;

namespace AuctionInitService.Model.Kafka
{
    public class KafkaOption
    {
        public List<int> Partitions { get; set; }
        public List<string> Hosts { get; set; }
        public string? GroupId { get; set; }
        public string? QueueIn { get; set; }
        public string? QueueOut { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }
        public string? Topics { get; set; }
        public SaslMechanism? Mechanism { get; set; }
        public SecurityProtocol? Protocol { get; set; }
    }
}
