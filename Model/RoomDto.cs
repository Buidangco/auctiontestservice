﻿namespace AuctionInitService.Model
{
    public class RoomDto
    {
        public Guid? RoomId { get; set; }

        /// <summary>
        /// Thời điểm tạo phòng
        /// </summary>
        public DateTime? CreatedAt { get; set; }

        public DateTime? StartTime { get; set; }

        public long UnixStartTime { get; set; }
        /// <summary>
        /// Thời điểm kết thúc đấu giá
        /// </summary>
        public DateTime? EndTime { get; set; }

        public long UnixEndTime { get; set; }

        /// <summary>
        /// Giá khởi điểm
        /// </summary>
        public decimal? EstimatePrice { get; set; }

        /// <summary>
        /// Bước giá tối thiểu
        /// </summary>
        public decimal? StepPrice { get; set; }

        /// <summary>
        /// Id lượt trả giá có giá tốt nhất
        /// </summary>
        public Guid? BestBidId { get; set; }

        /// <summary>
        /// Thời gian lượt trả giá tốt nhất
        /// </summary>
        public long BestBidTimestamp { get; set; }

        /// <summary>
        /// Id người trả giá tốt nhất
        /// </summary>
        public Guid? BestBidUserId { get; set; }

        /// <summary>
        /// Giá tốt nhất
        /// </summary>
        public decimal? BestBidPrice { get; set; }

        /// <summary>
        /// Vị trí bản ghi trên kafka
        /// </summary>
        public long BestOffset { get; set; }
    }
}
