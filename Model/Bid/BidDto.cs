﻿using AuctionInitService.Extensions;

namespace AuctionInitService.Model
{
    public class BidDto
    {
        public Guid Id { get; set; }
        public DateTime Timestamp { get; set; }
        public Guid? RoomId { get; set; }
        public string? UserId { get; set; }
        public decimal? Price { get; set; }
        public string? RoundName { get; set; }
        public decimal PriceMillion
        {
            get
            {
                if (!this.Price.HasValue || this.Price.Value == 0) return 0;
                return this.Price.Value / 1000000;
            }
        }
        public string? RequestId { get; set; }
        public override string ToString()
        {
            return $"{this.RoomId},{this.Timestamp.ToUnixTime()},{this.UserId},{(this.Price ?? 0).ToString()},{this.RequestId},{this.RoundName}";
        }
    }
}
