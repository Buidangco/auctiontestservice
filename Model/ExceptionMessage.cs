﻿using AuctionInitService.Common;

namespace AuctionInitService.Model
{
    public class ExceptionMessage
    {
        public ResultCode ErrorCode { get; set; }

        public string? Message { get; set; }
    }
}
