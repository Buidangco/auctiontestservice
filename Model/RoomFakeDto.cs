﻿namespace AuctionInitService.Model
{
    public class RoomFakeDto
    {
        public string id { get; set; }
        public string? startTime { get; set; }
       
        public string? endTime { get; set; }

        public decimal stepPrice { get; set; }

        public string? toppicName { get; set; }
    }
}
