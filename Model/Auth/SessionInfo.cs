﻿namespace AuctionInitService.Model
{
    public class UserSessionInfo
    {
        public string? UserId { get; set; }
    }
}
