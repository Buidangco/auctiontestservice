﻿namespace AuctionInitService.Model.UserBlock
{
    public class UserBlockRequest
    {
        public string[]? Tokens { get; set; }
    }
}
