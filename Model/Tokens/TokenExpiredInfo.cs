﻿namespace AuctionInitService.Model.Tokens
{
    public class TokenExpiredInfo
    {
        public List<string> Tokens { get; set; }
    }
}
