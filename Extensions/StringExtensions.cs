﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.Globalization;

namespace AuctionInitService.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNotNullOrEmpty(this string value)
        {
            return !string.IsNullOrEmpty(value);
        }

        public static int ToInt(this string value, int valueDefault)
        {
            if (value == null)
            {
                return valueDefault;
            }

            _ = int.TryParse(value.ToString(), out int outValue);
            return outValue;
        }

        public static T DeserializeJson<T>(this string jsonStr)
        {
            try
            {
                if (string.IsNullOrEmpty(jsonStr))
                {
                    return default;
                }
                return JsonConvert.DeserializeObject<T>(jsonStr);
            }
            catch
            {
                return default;
            }
        }

        public static string ConvertObjectToJson(this object value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            return JsonConvert.SerializeObject(value);
        }

        public static long ToUnixTime(this DateTime source)
        {
            return (long)(source - new DateTime(1970, 1, 1)).TotalMilliseconds;
        }

        public static string[] SpliptToArray(this string value, char seperate)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            return value.Split(seperate);
        }

        public static string GetItemInArray(this string[] value, int indexof)
        {
            if (value == null)
            {
                return string.Empty;
            }

            try
            {
                return value[indexof];
            }
            catch
            {
                return string.Empty;
            }
        }

        public static decimal ToDecimal(this string value, decimal defaultValue)
        {
            try
            {

                _ = decimal.TryParse(value, out defaultValue);
                return defaultValue;
            }
            catch
            {
                return defaultValue;
            }
        }

        public static List<List<string>> ChunkLoopList(this List<string> source, int chunkSize = 10)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }

        public static Guid ToGuid(this string value)
        {
            if (value.IsNullOrEmpty()) return Guid.Empty;
            try
            {
                return Guid.Parse(value);
            }
            catch (Exception)
            {

                return Guid.Empty;
            }
        }

        public static DateTime? ConvertToDateTime(this string value)
        {
            if (value == null)
            {
                return null;
            }
            return DateTime.Parse(value, CultureInfo.InvariantCulture);
        }
        public static bool ToBoolean(this string value)
        {
            return value.EmptyNull().ToLower() switch
            {
                "true" => true,
                "t" => true,
                "1" => true,
                "0" => false,
                "false" => false,
                "f" => false,
                "" => false,
                _ => false,
            };
        }
        public static string EmptyNull(this string str)
        {
            return str ?? "";
        }

    }
}
