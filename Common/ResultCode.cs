﻿namespace AuctionInitService.Common
{
    public enum ResultCode : int
    {
        NoErrorGetWay = 0,
        NoError = 1,
        DataInvalid,
        NotFound,
        ExistedSerial,
        UnknownError = 99,
    }
}
