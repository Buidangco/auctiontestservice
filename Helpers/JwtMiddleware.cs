namespace AuctionInitService.Helpers;
using AuctionInitService.BL;
using AuctionInitService.Model;
using AuctionInitService.Model;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

public class JwtMiddleware
{
    private readonly RequestDelegate _next;
    private readonly AppSettings _appSettings;
    private readonly IUserService _userService;
    public JwtMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings, IUserService userService)
    {
        _next = next;
        _appSettings = appSettings.Value;
        _userService = userService;
    }

    public async Task Invoke(HttpContext context)
    {
        var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
        if (token != null)
        {
          await AttachUserToContext(context, token);
        } 
        await _next(context);
    }

    private async Task AttachUserToContext(HttpContext context, string token)
    {
        try
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            tokenHandler.ValidateToken(token, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                ClockSkew = TimeSpan.Zero
            }, out SecurityToken validatedToken);

            var jwtToken = (JwtSecurityToken)validatedToken;
            string userId = jwtToken.Claims.First(p => p.Type.Equals("id")).Value;
            bool tokenIsExpired = await _userService.TokenIsExpired(userId);
            if (tokenIsExpired) return;
            context.Items["User"] = new User()
            {
                Id = userId,
            }; 
        }
        catch(Exception ex)
        {
            Console.WriteLine(ex.ToString());   
            // do nothing if jwt validation fails
            // user is not attached to context so request won't have access to secure routes
        }
    }    
}