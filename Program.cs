using AuctionInitService.BL;
using AuctionInitService.Helpers;
using AuctionInitService.Model;
using AuctionInitService.Model.Kafka;
using AuctionInitService.Model.MogoDB;
using AuctionInitService.ServiceExtension;
using NLog;
using NLog.Web;

var logger = NLog.LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
try
{
	var builder = WebApplication.CreateBuilder(args);
	builder.Services.AddIServicesScoped(builder);

	builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
	// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
	builder.Services.AddControllers();
	//builder.Services.AddEndpointsApiExplorer();
	builder.Services.AddSwaggerGen();
	//builder.Services.AddTransient<OperationLogHandler>();
	builder.Services.Configure<KafkaOption>(builder.Configuration.GetSection("Kafka"));
	builder.Services.Configure<ActionLogDatabaSettings>(builder.Configuration.GetSection("ActionLogDatabase"));
	builder.Services.Configure<AppSettings>(builder.Configuration.GetSection("AppSettings"));
	builder.Services.AddSingleton<ActionMemoryCache>();
    builder.Logging.ClearProviders();
	builder.Host.UseNLog();
    var app = builder.Build();
	// Configure the HTTP request pipeline.
	//if (app.Environment.IsDevelopment())
	//{
	app.UseSwagger();
	app.UseSwaggerUI();
	//}
	app.UseMiddleware<JwtMiddleware>();
	app.UseAuthentication();
	app.UseAuthorization();
	app.MapControllers();

	app.Run();
}
catch (Exception ex)
{
	logger.Error(ex);
}
finally
{
    NLog.LogManager.Shutdown();
}
