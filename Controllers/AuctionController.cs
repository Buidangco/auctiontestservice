﻿using AuctionInitService.BL;
using AuctionInitService.Helpers;

using Microsoft.AspNetCore.Mvc;

namespace AuctionInitService.Controllers
{
    [ApiController]
    [Route("auction")]
    public class AuctionController : BaseController
    {
        private readonly IBidService _bidService;

        public AuctionController(IBidService bidService)
        {
            _bidService = bidService;
        }

        [Authorize]
        [HttpPost("test")]
        public async Task CreateTestBid()
        {
        }
    }
}
