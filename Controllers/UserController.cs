﻿using AuctionInitService.BL;
using AuctionInitService.Model.UserBlock;
using Microsoft.AspNetCore.Mvc;

namespace AuctionInitService.Controllers
{
    [ApiController]
    [Route("api/v1/user")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService) 
        {
            _userService = userService;
        }

        [HttpPost("{userId}/block")]
        public async Task<IActionResult> BlockUser(string userId, UserBlockRequest blockRequest)
        {
            if(userId == null || blockRequest == null)
            {
                return StatusCode(400, "Bad request");
            }
            await _userService.BlockUser(userId, blockRequest.Tokens);
            return Ok();
        }
    }
}
