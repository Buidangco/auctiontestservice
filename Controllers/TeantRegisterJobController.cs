﻿using Microsoft.AspNetCore.Mvc;
using AuctionJob.BL;
using AuctionJob.Model;

namespace ManagemantCache.Controllers
{
    [ApiController]
    [Route("room-service")]
    public class RoomController : ControllerBase
    {
        private readonly ILogger<TeantRegisterJobController> _logger;
        private readonly ITeantRegisterJobService _teantRegisterJobService;
        public TeantRegisterJobController(ILogger<TeantRegisterJobController> logger,
            ITeantRegisterJobService teantRegisterJobService)
        {
            _logger = logger;
            _teantRegisterJobService = teantRegisterJobService;   
        }

        [HttpGet("detail")]
        public async Task<TenantRegisterJobDto> GetDetail(Guid tenantId)
        {
            var tenantRegisterJobDto = await _teantRegisterJobService.GetDetail(tenantId);
            return tenantRegisterJobDto;
        }

        [HttpPost]
        public async Task<TenantRegisterJobDto> Create(TenantRegisterJobDto tenantRegisterJobDto)
        {
            return await _teantRegisterJobService.Create(tenantRegisterJobDto);
        }

        [HttpPost("update")]
        public async Task<TenantRegisterJobDto> Update(TenantRegisterJobDto tenantRegisterJobDto)
        {
            return await _teantRegisterJobService.Update(tenantRegisterJobDto);
        }

        [HttpGet("job-register")]
        public async Task<List<TenantRegisterJobDto>> GetJobOfTenant(string? taxCode = null, string? version = null, string? serial = null, string? emailTo = null)
        {
            return await _teantRegisterJobService.Filter(new TenantSearchJobDto() { TaxCode = taxCode, Version = version, EmailTo = emailTo, Serial = serial});
        }

        [HttpDelete]
        public async Task Delete(Guid tenantId)
        {
            await _teantRegisterJobService.Delete(tenantId);
        }

    }
}
