﻿using Microsoft.AspNetCore.Mvc;
using AuctionJob.BL;

namespace AuctionJob.Controllers
{
    [ApiController]
    [Route("hang-fire")]
    public class HangFireJobController : ControllerBase
    {
        private readonly IHangFireJobService _hangfireService;
        public HangFireJobController(IHangFireJobService vendorService)
        {
            _hangfireService = vendorService;   
        }

        [HttpPost("start")]
        public void CreateStartService(string jobName, string parameter, string cronExpression)
        {
            _hangfireService.Start(jobName, parameter, cronExpression);
        }

        [HttpPost("stop")]
        public void CreateStopService()
        {
            _hangfireService.Stop();
        }

        [HttpPost("restart")]
        public void CreateRestart()
        {
            _hangfireService.Restart();
        }

    }
}
