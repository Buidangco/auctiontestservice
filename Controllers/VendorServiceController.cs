﻿using Microsoft.AspNetCore.Mvc;
using MinvoiceCronJob.BL;
using MinvoiceCronJob.Model;

namespace ManagemantCache.Controllers
{
    [ApiController]
    [Route("vendor-service")]
    public class VendorServiceController : ControllerBase
    {
        private readonly ILogger<VendorServiceController> _logger;
        private readonly IVendorService _vendorService;
        public VendorServiceController(ILogger<VendorServiceController> logger,
            IVendorService vendorService)
        {
            _logger = logger;
            _vendorService = vendorService;   
        }

        [HttpPost("create-authority-tax-code")]
        public async Task<StandardResponce> CreateTaxAuthorityCode(StandardRequest request)
        {
            var no = await _vendorService.Create(request);
            return no;
        }

    }
}
