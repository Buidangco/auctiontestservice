﻿using AuctionInitService.BL.Interface;
using AuctionInitService.Model.Room;
using Microsoft.AspNetCore.Mvc;

namespace AuctionInitService.Controllers
{
    [ApiController]
    [Route("api/v1/rooms")]
    public class RoomController : ControllerBase
    {
        private readonly IRoomService _roomService;
        public RoomController(IRoomService roomService) 
        {
            _roomService = roomService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateRoom(List<RoomRequest> roomRequests)
        {
            if (roomRequests == null)
            {
                return StatusCode(400, "Bad request");
            }
            await _roomService.CreateRoom(roomRequests);
            return Ok();
        }

        [HttpPost("{roomId}/join")]
        public async Task<IActionResult> GetRoom(string roomId, List<JoinRoomRequest> roomRequests)
        {

            if (roomRequests == null || string.IsNullOrEmpty(roomId))
            {
                return StatusCode(400, "Bad request");
            }
            await _roomService.JoinRoom(roomId, roomRequests);
            return Ok();
        }

        [HttpGet("{roomId}/history")]
        public async Task<IActionResult> HistoryRoom(string roomId)
        {
            if (string.IsNullOrEmpty(roomId))
            {
                return StatusCode(400, "Bad request");
            }
            var data = await _roomService.GetHistory(roomId);
            return Ok(data);
        }
    }
}
