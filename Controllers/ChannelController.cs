﻿using AuctionInitService.Model.Channel;
using Microsoft.AspNetCore.Mvc;

namespace AuctionInitService.Controllers
{
    [ApiController]
    [Route("/api/v1/channel")]
    public class ChannelController : ControllerBase
    {
        public ChannelController() { }

        [HttpPost("{channelId}/join")]
        public async Task<IActionResult> CreateChannel(string channelId,ChannelRequest channelRequest)
        {
            if (channelRequest == null || String.IsNullOrEmpty(channelId))
            {
                return StatusCode(400, "Bad request");
            }
            return Ok();
        }
    }
}
