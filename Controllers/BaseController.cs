﻿using AuctionInitService.Model;
using Microsoft.AspNetCore.Mvc;

namespace AuctionInitService.Controllers
{
    public class BaseController : ControllerBase
    {
        private string? currentUserId;

        private string? currentRequestId;

        private string? roundName;

        public string CurrentUserId
        {
            get
            {
                if (string.IsNullOrEmpty(this.currentUserId))
                {
                    this.currentUserId = GetCurrentUser();
                }

                return this.currentUserId;
            }
        }

        public string CurrentRequestId
        {
            get
            {
                if (string.IsNullOrEmpty(this.currentRequestId))
                {
                    this.currentRequestId = GetXRequestId();
                }

                return this.currentRequestId;
            }
        }

        public string? RoundName
        {
            get
            {
                if (string.IsNullOrEmpty(this.roundName))
                {
                    this.roundName = GetRoundName();
                }

                return this.roundName;
            }
        }

        private string GetCurrentUser()
        {
            if (Request == null)
            {
                return string.Empty;
            }
            return GetAuthenticatedToken();


        }

        private string GetAuthenticatedToken()
        {
            var user = (User)Request.HttpContext.Items["User"];
            return user?.Id;
        }
        ///X-Request-Id

        private string GetXRequestId()
        {
            string xrequestId = Request.Headers["X-Request-Id"].FirstOrDefault()?.Split(" ").Last();
            if (!string.IsNullOrWhiteSpace(xrequestId)) return xrequestId;
            return Guid.NewGuid().ToString();
        }

        private string GetRoundName()
        {
            string xrequestId = Request.Headers["roundName"].FirstOrDefault()?.Split(" ").Last();
            if (!string.IsNullOrWhiteSpace(xrequestId)) return xrequestId;
            return Guid.NewGuid().ToString();
        }



    }
}
