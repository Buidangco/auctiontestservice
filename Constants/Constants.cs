﻿namespace AuctionInitService.Constants
{
    public static class COMMON
    {
        public const string PrefixCacheKey = "auction:";
        public const string PrefixCacheKeyAuth = "authencation:";
        public const string TokenCacheKey = "Tokens";
        public const string RoomCacheKey = "Rooms";
        public const string UserRoomsCacheKey = "UserRooms";
    }
}
