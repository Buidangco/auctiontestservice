﻿using AuctionInitService.Extensions;
using AuctionInitService.Model;
using AuctionInitService.Model.MogoDB;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace AuctionInitService.BL
{
    public class ActionlogService : IActionlogService
    {
        private readonly ILogger<ActionlogService> _logger;
        private readonly IMongoCollection<ActionLog> _actionlogCollection;

        public ActionlogService(ILogger<ActionlogService> logger,
             IOptions<ActionLogDatabaSettings> option)
        {
            _logger = logger;
            var mongoClient = new MongoClient(option.Value.ConnectionString);
            var mongoDatabase = mongoClient.GetDatabase(option.Value.DatabaseName);
            _actionlogCollection = mongoDatabase.GetCollection<ActionLog>(option.Value.CollectionName);
        }

        public async Task CreateAsync(ActionLog actionLog)
        {
            if (actionLog == null) return;
            try
            {
                await _actionlogCollection.InsertOneAsync(actionLog);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Create log Error {ActionLog}", actionLog.ConvertObjectToJson());
            }
        }       
    }
}
