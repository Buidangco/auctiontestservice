﻿using AuctionInitService.BL.Interface;
using AuctionInitService.Constants;
using AuctionInitService.Extensions;
using AuctionInitService.Model;
using AuctionInitService.Model.Room;

namespace AuctionInitService.BL
{
    public class RoomService : IRoomService
    {
        private readonly ILogger<RoomService> _logger;
        private readonly IAuthDistributedCache _cacheAuthService;
        private readonly IActionDistributedCache _cacheActionService;
        private readonly IRedisCacheService _redisCacheService;
        public RoomService(ILogger<RoomService> logger,
                           IActionDistributedCache cacheService,
                           IAuthDistributedCache cacheAuthService,
                           IRedisCacheService redisCacheService)
        {
            _logger = logger;
            _cacheAuthService = cacheAuthService;
            _cacheActionService = cacheService;
            _redisCacheService = redisCacheService;
        }

        public async Task CreateRoom(List<RoomRequest> roomRequests)
        {
            await SetRoomOnCache(roomRequests);
        }
        public async Task JoinRoom(string roomId, List<JoinRoomRequest> joinRooms)
        {
            await SetUserRoomOnCache(roomId, joinRooms);
        }

        public async Task<List<HistoryRoomReponse>> GetHistory(string roomId)
        {
            var historyBid = await _redisCacheService.GetRangeAsync<HistoryRoomReponse>(roomId, 0, 100);
            return historyBid;
        }

        private async Task SetRoomOnCache(List<RoomRequest> roomRequests)
        {
            string cacheKey = COMMON.RoomCacheKey;
            Dictionary<string, string> rooms = new();
            foreach (var room in roomRequests)
            {
                if (rooms.ContainsKey(room.Id)) continue;
                rooms.Add(room.Id, room.ConvertObjectToJson());
                await CreateRoom(room);
            }

            await _cacheAuthService.HashSetAsync(cacheKey, rooms);
        }

        public async Task CreateRoom(RoomRequest roomDto)
        {
            try
            {
                string cacheKey = (roomDto.Id ?? string.Empty);
                var cacheData = CreateHashDataCache(roomDto);
                await _cacheActionService.HashSetAsync(cacheKey, cacheData);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Lỗi trong quá trình tạo cache phòng đấu {data}", roomDto.ConvertObjectToJson());
            }
        }

        private static Dictionary<string, string> CreateHashDataCache(RoomRequest roomDto)
        {
            return new Dictionary<string, string>()
             {
                 { nameof(RoomDto.StartTime), (roomDto.StartTime ?? DateTime.UtcNow).ToString()},
                 { nameof(RoomDto.UnixStartTime), ((DateTimeOffset)(roomDto.StartTime ?? DateTime.UtcNow)).ToUnixTimeMilliseconds().ToString()},
                 { nameof(RoomDto.EndTime), (roomDto.EndTime ?? DateTime.UtcNow).ToString()},
                 { nameof(RoomDto.UnixEndTime), ((DateTimeOffset)(roomDto.EndTime ?? DateTime.UtcNow)).ToUnixTimeMilliseconds().ToString()},
                 { nameof(RoomDto.EstimatePrice), (roomDto?.EstimatePrice ?? 0).ToString()},
                 { nameof(RoomDto.StepPrice), (roomDto?.StepPrice ?? 0).ToString()},
                 { nameof(RoomDto.BestBidId), Guid.Empty.ToString() },
                 { nameof(RoomDto.BestBidTimestamp), "0"},
                 { nameof(RoomDto.BestBidUserId), Guid.Empty.ToString() },
                 { nameof(RoomDto.BestBidPrice), (roomDto?.StepPrice ?? 0).ToString()},
                 { nameof(RoomDto.BestOffset),  "0"},
             };
        }

        private async Task SetUserRoomOnCache(string roomId, List<JoinRoomRequest> roomJoins)
        {
            string cacheKey = COMMON.UserRoomsCacheKey;
            Dictionary<string, string> rooms = BuildUserRoom(roomId, roomJoins);
            await _cacheAuthService.HashSetAsync(cacheKey, rooms);
        }

        private static Dictionary<string, string> BuildUserRoom(string roomId, List<JoinRoomRequest> roomJoins)
        {
            Dictionary<string, string> rooms = new();

            foreach (var item in roomJoins)
            {
                string key = $"{roomId}_{item.UserId}";
                if (rooms.ContainsKey(key)) continue;
                rooms.Add(key, item.ConvertObjectToJson());
            }
            return rooms;
        }
    }
}
