﻿using AuctionInitService.BL.Interface;
using AuctionInitService.Constants;
using AuctionInitService.Model;

namespace AuctionInitService.BL
{
    public class UserService : IUserService
    {
        private readonly ILogger<UserService> _logger;
        private readonly IAuthDistributedCache _cacheService;
        public UserService(ILogger<UserService> logger,
              IAuthDistributedCache cacheService)
        {
            _logger = logger;
            _cacheService = cacheService;
        }

        public async Task<User> Authenticate(string userId)
        {
            var user = new User(); //_users.SingleOrDefault(x => x.Username == model.Username && x.Password == model.Password);

            // return null if user not found
            if (user == null) return null;
            return new();
        }

        public async Task BlockUser(string userId,string[] tokens)
        {
            await SetUserToken(userId, tokens);
        }

        public User GetById(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> TokenIsExpired(string userId)
        {
            string cacheKey = $"{COMMON.PrefixCacheKeyAuth}{COMMON.TokenCacheKey}";
            string value = await _cacheService.HashGetAsync(cacheKey, userId);
            return !string.IsNullOrEmpty(value);
        }

        private async Task SetUserToken(string userId, string[] tokens)
        {
            string cacheKey = $"{COMMON.PrefixCacheKeyAuth}{COMMON.TokenCacheKey}";

            string tokenJoin = string.Empty;

            if(tokens.Length > 0)
            {
                tokenJoin = String.Join(',', tokens);
            }
            Dictionary<string, string> tokenBlocks = new()
            {
                { userId,  tokenJoin }
            };
            await _cacheService.HashSetAsync(cacheKey, tokenBlocks);
        }
    }
}
