﻿using AuctionInitService.Model;
using AuctionInitService.Model.Tokens;

namespace AuctionInitService.BL
{
    public interface IBidService
    {
        Task CreateBit(Guid? id,BidDto roomDto);
        Task CreateRoom(RoomDto roomDto);

        Task<List<RoomDto>> GetRooms();

        Task CreateTokenExpired(TokenExpiredInfo tokenExpired);

        Task<string> GetTokenExpired(string userId);
        Task<string> FakeData(string userId);


    }
}
