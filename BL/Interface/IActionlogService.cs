﻿using AuctionInitService.Model;

namespace AuctionInitService.BL
{
    public interface IActionlogService
    {
        Task CreateAsync(ActionLog actionLog);     
    }
}
