﻿using Microsoft.Extensions.Caching.Distributed;

namespace AuctionInitService.BL.Interface
{
    public interface IActionDistributedCache : ICacheService { }
}
