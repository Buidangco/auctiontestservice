﻿using AuctionInitService.Model.Room;

namespace AuctionInitService.BL.Interface
{
    public interface IRoomService
    {
        Task CreateRoom(List<RoomRequest> roomRequests);
        Task JoinRoom(string roomId, List<JoinRoomRequest> roomRequests);
        Task<List<HistoryRoomReponse>> GetHistory(string roomId);
    }
}
