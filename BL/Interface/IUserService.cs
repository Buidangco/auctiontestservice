﻿using AuctionInitService.Model;

namespace AuctionInitService.BL
{
    public interface IUserService
    {
        Task<User> Authenticate(string userId);
        Task<bool> TokenIsExpired(string userId);
        Task BlockUser(string userId, string[] tokens);
    }
}
