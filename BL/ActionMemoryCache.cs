﻿using Microsoft.Extensions.Caching.Memory;

namespace AuctionInitService.BL
{
    public class ActionMemoryCache
    {
      public MemoryCache Cache { get; } = new MemoryCache(
       new MemoryCacheOptions
       {
           SizeLimit = 2048
       });
    }
}
