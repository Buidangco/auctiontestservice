﻿using Amazon.Runtime.Internal.Transform;
using AuctionInitService.BL.Interface;
using AuctionInitService.Common;
using AuctionInitService.Constants;
using AuctionInitService.Extensions;
using AuctionInitService.Model;
using AuctionInitService.Model.Kafka;
using AuctionInitService.Model.Tokens;
using Confluent.Kafka;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;

namespace AuctionInitService.BL
{
    public class BidService : IBidService
    {
        private readonly ILogger<BidService> _logger;
        private readonly KafkaOption _option;
        private readonly ProducerConfig _producerConfig;
        private readonly IActionDistributedCache _cacheService;
        private readonly IAuthDistributedCache _cacheAuthService;
        private readonly AppSettings _appSettings;
        private readonly ActionMemoryCache _actionMemoryCache;
        private readonly TimeSpan _cacheTimeout;
        private readonly IProducer<Null, string> _producerBuilder;
        public BidService(ILogger<BidService> logger,
            IOptions<KafkaOption> option,
            IActionDistributedCache cacheService,
            IAuthDistributedCache cacheAuthService,
            IOptions<AppSettings> appSettings,
            ActionMemoryCache actionMemoryCache)
        {
            _logger = logger;
            _option = option.Value;
            _cacheService = cacheService;
            _cacheAuthService = cacheAuthService;
            _appSettings = appSettings.Value;
            _producerConfig = new ProducerConfig
            {
                BootstrapServers = string.Join(",", _option.Hosts),
                ClientId = Dns.GetHostName(),
            };
            _actionMemoryCache = actionMemoryCache;
            _cacheTimeout = TimeSpan.FromMinutes(1);
            _producerBuilder = new ProducerBuilder<Null, string>(_producerConfig)
                                    .SetValueSerializer(Serializers.Utf8)
                                    .Build();
        }
        public async Task CreateRoom(RoomDto roomDto)
        {
            try
            {
                string cacheKey = $"{COMMON.PrefixCacheKey}{(roomDto.RoomId ?? Guid.Empty)}";
                var cacheData = CreateHashDataCache(roomDto);
                await _cacheService.HashSetAsync(cacheKey, cacheData);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Lỗi trong quá trình tạo cache phòng đấu {data}", roomDto.ConvertObjectToJson());
            }
        }

        public async Task CreateBit(Guid? id, BidDto roomDto)
        {
            var (isValid, messaggeError) = await ValidRoomData(roomDto);
            if (!isValid)
            {
                throw new BusinessLogicException(ResultCode.DataInvalid, messaggeError);
            }

            string? toppicName = await GetToppicNameOfRoom((roomDto?.RoomId ?? Guid.Empty).ToString(), roomDto?.UserId);
            if (string.IsNullOrEmpty(toppicName))
            {
                throw new BusinessLogicException(ResultCode.DataInvalid, "User không tồn tại vui lòng kiểm tra lại");
            }
            WriteLogTrace(roomDto.RoundName, roomDto.RequestId, (roomDto.RoomId ?? Guid.Empty).ToString(), roomDto.UserId);
            await ProduceMessageAsync(roomDto, toppicName);
        }

        public async Task<List<RoomDto>> GetRooms()
        {
            var listKey = new List<string>() { "3fa85f64-5717-4562-b3fc-2c963f66afa6", "3a03dbc3-7bf8-dece-0f36-ce03318241ed" };
            foreach (var item in listKey)
            {
                string cacheKey = $"{COMMON.PrefixCacheKey}{(item)}";
                var roomItem = _cacheService.HashGetAsync(cacheKey);
            }
            return new();
        }

        private async Task ProduceMessageAsync(BidDto roomDto, string toppicName)
        {
            try
            {
                var result = await _producerBuilder.ProduceAsync(toppicName, new Message<Null, string>
                {
                    Value = roomDto.ToString()
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to produce {Data}", roomDto.ConvertObjectToJson());
            }
        }

        private static Dictionary<string, string> CreateHashDataCache(RoomDto roomDto)
        {
            return new Dictionary<string, string>()
             {
                 { nameof(RoomDto.CreatedAt), DateTime.UtcNow.ToString()},
                 { nameof(RoomDto.StartTime), (roomDto.StartTime ?? DateTime.UtcNow).ToString()},
                 { nameof(RoomDto.UnixStartTime), ((DateTimeOffset)(roomDto.StartTime ?? DateTime.UtcNow)).ToUnixTimeMilliseconds().ToString()},
                 { nameof(RoomDto.EndTime), (roomDto.EndTime ?? DateTime.UtcNow).ToString()},
                 { nameof(RoomDto.UnixEndTime), ((DateTimeOffset)(roomDto.EndTime ?? DateTime.UtcNow)).ToUnixTimeMilliseconds().ToString()},
                 { nameof(RoomDto.EstimatePrice), (roomDto?.EstimatePrice ?? 0).ToString()},
                 { nameof(RoomDto.StepPrice), (roomDto?.StepPrice ?? 0).ToString()},
                 { nameof(RoomDto.BestBidId), Guid.Empty.ToString() },
                 { nameof(RoomDto.BestBidTimestamp), "0"},
                 { nameof(RoomDto.BestBidUserId), Guid.Empty.ToString() },
                 { nameof(RoomDto.BestBidPrice), (roomDto?.BestBidPrice ?? 0).ToString()},
             };
        }
        public async Task CreateTokenExpired(TokenExpiredInfo tokenExpired)
        {
            string cacheKey = $"{COMMON.PrefixCacheKeyAuth}{COMMON.TokenCacheKey}";
            var dcTokenExpried = new Dictionary<string, string>();
            foreach (var token in tokenExpired.Tokens)
            {
                string userId = GetUserId(token);
                if (dcTokenExpried.ContainsKey(userId)) continue;
                dcTokenExpried.Add(userId, token);
            }
            await _cacheAuthService.HashSetAsync(cacheKey, dcTokenExpried);
        }


        private string GetUserId(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            tokenHandler.ValidateToken(token, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero
            }, out SecurityToken validatedToken);

            var jwtToken = (JwtSecurityToken)validatedToken;
            return jwtToken.Claims.First(p => p.Type == "id").Value;
        }

        private void WriteLogTrace(string roundName, string requesId, string roomId, string userId)
        {
            _logger.LogDebug("{Timestamp} | {ServiceName} | {RoundName} | {RequestId} | {RoomId} | {UserId}", DateTime.UtcNow.ToUnixTime(), "BiddingService", roundName, requesId, roomId, userId);
        }

        public async Task<string> GetTokenExpired(string userId)
        {
            return GenerateJwtToken(userId);
        }

        private string GenerateJwtToken(string userId)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", userId) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                Claims = new Dictionary<string, object>() { { JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N") } }
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        private async Task<(bool, string)> ValidRoomData(BidDto bidDto)
        {
            var roomDto = await GetRoomAsync((bidDto?.RoomId ?? Guid.Empty).ToString());

            if (bidDto.PriceMillion < (roomDto.StepPrice ?? 0))
            {
                return (false, "Giá đấu không hợp lệ");
            }

            return (true, string.Empty);
        }

        private async Task<RoomDto> GetRoomAsync(string roomId)
        {
            string cacheKey = $"{COMMON.PrefixCacheKeyAuth}{COMMON.RoomCacheKey}";
            return await GetRoomCache<RoomDto>(cacheKey, roomId);
        }
        public async Task<string> FakeData(string userId)
        {
            var userTokens = ReadFile("Data_Test_Users.csv");
            var userRooms = BuildRooms();
            await SetRoomOnCache(userRooms);
            await SetUserRoomOnCache(userTokens, userRooms);
            await SetTokenOnCache();
            await CreateRoom(userRooms);
            return string.Empty;
        }

        private async Task CreateRoom(List<RoomFakeDto> rooms)
        {
            foreach (var item in rooms)
            {
                await CreateRoom(new RoomDto()
                {
                    RoomId = item.id.ToGuid(),
                    StartTime = item.startTime.ConvertToDateTime(),
                    EndTime = item.endTime.ConvertToDateTime(),
                    StepPrice = item.stepPrice,
                    BestBidPrice = item.stepPrice,
                });
            }
        }

        private List<RoomFakeDto> BuildRooms()
        {
            var datas = ReadFile("Data_Test_Rooms.csv");
            var listRoom = new List<RoomFakeDto>();
            foreach (var item in datas)
            {
                var array = item.SpliptToArray(',');
                if (array == null) continue;
                listRoom.Add(new RoomFakeDto()
                {
                    id = array.GetItemInArray(0),
                    startTime = array.GetItemInArray(1),
                    endTime = array.GetItemInArray(2),
                    stepPrice = (array.GetItemInArray(3).ToDecimal(0) / 1000000),
                    toppicName = array.GetItemInArray(5),
                });
            }

            return listRoom;
        }

        private async Task SetRoomOnCache(List<RoomFakeDto> roomFakes)
        {
            string cacheKey = $"{COMMON.PrefixCacheKeyAuth}{COMMON.RoomCacheKey}";
            Dictionary<string, string> rooms = new();
            foreach (var room in roomFakes)
            {
                if (rooms.ContainsKey(room.id)) continue;
                rooms.Add(room.id, room.ConvertObjectToJson());
            }
            await _cacheAuthService.HashSetAsync(cacheKey, rooms);
        }

        private async Task SetTokenOnCache()
        {
            string cacheKey = $"{COMMON.PrefixCacheKeyAuth}{COMMON.TokenCacheKey}";
            var datas = ReadFile("Data_Test_Tokens.csv");
            var chunks = datas.ChunkLoopList(10000);
            foreach (var token in chunks)
            {
                var tokenDistinct = token.Distinct().ToDictionary(k => k, v => v);
                await _cacheAuthService.HashSetAsync(cacheKey, tokenDistinct);
            }
        }
        private async Task SetUserRoomOnCache(List<string> tokens, List<RoomFakeDto> roomFakes)
        {
            string cacheKey = $"{COMMON.PrefixCacheKeyAuth}{COMMON.UserRoomsCacheKey}";
            Dictionary<string, string> rooms = new();
            foreach (var room in tokens)
            {
                var roomtest = BuildUserRoom(room, roomFakes);
                await _cacheAuthService.HashSetAsync(cacheKey, roomtest);
            }
            await _cacheAuthService.HashSetAsync(cacheKey, rooms);
        }

        private Dictionary<string, string> BuildUserRoom(string userId, List<RoomFakeDto> roomFakes)
        {
            Dictionary<string, string> rooms = new();
            foreach (var item in roomFakes)
            {
                string key = $"{item.id}_{userId}";
                if (rooms.ContainsKey(key)) continue;
                rooms.Add(key, item?.toppicName);
            }
            return rooms;
        }

        public static List<string> ReadFile(string filename)
        {
            var path = @"D:/Data/";
            string fullPathFile = Path.Combine(path, filename);
            return System.IO.File.ReadAllLines(fullPathFile, Encoding.Default).ToList();
        }

        private void SetRoomToMemoryCache(string keyCache, string roomId, string value)
        {
            CancellationTokenSource cancellationTokenSource = new(_cacheTimeout);
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .AddExpirationToken(
                    new CancellationChangeToken(cancellationTokenSource.Token))
                .SetSize(value.Length);
            _actionMemoryCache.Cache.Set<string>($"{keyCache}_{roomId}", value, cacheEntryOptions);
        }

        private async Task<T> GetRoomCache<T>(string cacheKey, string roomId)
        {
            var value = _actionMemoryCache.Cache.Get<string>($"{cacheKey}_{roomId}");
            if (!value.IsNullOrEmpty()) return value.DeserializeJson<T>();
            value = await _cacheAuthService.HashGetAsync(cacheKey, roomId);
            if (value.IsNullOrEmpty()) return default(T);
            SetRoomToMemoryCache(cacheKey, roomId, value);
            return value.DeserializeJson<T>();
        }

        private void SetRoomOfUserMemoryCache(string keyCache, string subfix, string value)
        {
            CancellationTokenSource cancellationTokenSource = new(_cacheTimeout);
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .AddExpirationToken(
                    new CancellationChangeToken(cancellationTokenSource.Token))
                .SetSize(value.Length);
            _actionMemoryCache.Cache.Set<string>($"{keyCache}_{subfix}", value, cacheEntryOptions);
        }

        private async Task<string> GetToppicNameOfRoom(string roomId, string userId)
        {
            string cacheKey = $"{COMMON.PrefixCacheKeyAuth}{COMMON.UserRoomsCacheKey}";
            string subfixKey = $"{roomId}_{userId}";
            var value = _actionMemoryCache.Cache.Get<string>($"{cacheKey}_{subfixKey}");
            if (!value.IsNullOrEmpty()) return value;
            value = await _cacheAuthService.HashGetAsync(cacheKey, subfixKey);
            SetRoomOfUserMemoryCache(cacheKey, subfixKey, value);
            return value;
        }
    }
}
