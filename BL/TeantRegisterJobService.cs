﻿using AutoMapper;
using Hangfire;
using MinvoiceCronJob.Common;
using MinvoiceCronJob.DBAccessor;
using MinvoiceCronJob.Entities;
using MinvoiceCronJob.Model;

namespace MinvoiceCronJob.BL
{
    public class TeantRegisterJobService : ITeantRegisterJobService
    {
        private readonly ITenantRegisterJobRepository _tenantRegisterJobRepository;
        private readonly IHangFireJobService _hangFireJobService;
        private readonly IMapper _mapper;
        public TeantRegisterJobService(ITenantRegisterJobRepository
            tenantRegisterJobRepository,
            IHangFireJobService hangFireJobService,
            IMapper mapper)
        {
            _tenantRegisterJobRepository = tenantRegisterJobRepository;
            _mapper = mapper;
            _hangFireJobService = hangFireJobService;
        }
        public async Task<List<TenantRegisterJobDto>> Filter(TenantSearchJobDto model)
        {
            if (model == null)
            {
                throw new BusinessLogicException(ResultCode.DataInvalid, "Dữ liệu không hợp lệ hoặc null");
            }
            var tenantRegister = await _tenantRegisterJobRepository.Filter(model);
            return tenantRegister.Select(p => _mapper.Map<TenantRegisterJob, TenantRegisterJobDto>(p)).ToList();
        }

        public async Task<TenantRegisterJobDto> Create(TenantRegisterJobDto model)
        {
            if (model == null)
            {
                throw new BusinessLogicException(ResultCode.DataInvalid, "Dữ liệu không hợp lệ hoặc null");
            }

            var existed = await _tenantRegisterJobRepository.ExistedSerial(model?.Serial);
            if (existed)
            {
                throw new BusinessLogicException(ResultCode.ExistedSerial, "Đã tồn tại mẫu số ký hiệu");
            }

            var tenantRegisterJob = _mapper.Map<TenantRegisterJobDto, TenantRegisterJob>(model);
            tenantRegisterJob.Id = Guid.NewGuid();
            await _tenantRegisterJobRepository.Insert(tenantRegisterJob);
            var tenantRegisterJobDto = _mapper.Map<TenantRegisterJob, TenantRegisterJobDto>(tenantRegisterJob);
            //_hangFireJobService.CreateUpdateJobOfTenant(tenantRegisterJobDto);
            return tenantRegisterJobDto;
        }

        public async Task<TenantRegisterJobDto> Update(TenantRegisterJobDto model)
        {
            if (model == null)
            {
                throw new BusinessLogicException(ResultCode.DataInvalid, "Dữ liệu không hợp lệ hoặc null");
            }
            var currentTenantRegister = await _tenantRegisterJobRepository.GetById(model.Id);
            if (currentTenantRegister == null)
            {
                throw new BusinessLogicException(ResultCode.NotFound, "Không tìm hấy job vui lòng kiểm tra lại");
            }
            _mapper.Map(model, currentTenantRegister);
            await _tenantRegisterJobRepository.Update(currentTenantRegister);
            var tenantRegisterJobDto = _mapper.Map<TenantRegisterJob, TenantRegisterJobDto>(currentTenantRegister);
            //_hangFireJobService.CreateUpdateJobOfTenant(tenantRegisterJobDto);
            return tenantRegisterJobDto;
        }
        public async Task<bool> Delete(Guid teantId)
        {
            var tenantRegister = await _tenantRegisterJobRepository.GetById(teantId);
            if (tenantRegister == null)
            {
                throw new BusinessLogicException(ResultCode.NotFound, "Can't find tenant");
            }

            var result = await _tenantRegisterJobRepository.Delete(tenantRegister);
            var tenantRegisterJobDto = _mapper.Map<TenantRegisterJob, TenantRegisterJobDto>(tenantRegister);
            //_hangFireJobService.DeleteJobOfTenant(tenantRegisterJobDto);
            return result;

        }

        public async Task<TenantRegisterJobDto> GetDetail(Guid id)
        {
            var tenantRegister = await _tenantRegisterJobRepository.GetById(id);
            if (tenantRegister == null)
            {
                throw new BusinessLogicException(ResultCode.NotFound, "Can't find tenant");
            }

            return _mapper.Map<TenantRegisterJob, TenantRegisterJobDto>(tenantRegister);
        }

        private async Task<TenantRegisterJob> GetTenantRegister(string taxCode)
        {
            var tenantRegister = await _tenantRegisterJobRepository.GetDetailByTenant(taxCode);
            return tenantRegister;
        }

        public async Task<TenantRegisterJobDto> Filter()
        {
            var tenantRegister = await _tenantRegisterJobRepository.Filter(DateTime.Now);
            return new();
        }

    }
}
