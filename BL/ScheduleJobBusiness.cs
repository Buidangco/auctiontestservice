﻿using MinvoiceCronJob.Constants;
using MinvoiceCronJob.DBAccessor;
using MinvoiceCronJob.Entities;
using MinvoiceCronJob.Helpers;
using MinvoiceCronJob.Model;
using MinvoiceCronJob.RestAPI;

namespace MinvoiceCronJob.BL
{
    public class ScheduleJobBusiness
    {
        public static readonly ScheduleJobBusiness Instance = new();
        public readonly DbContextClass _context;
        //public readonly ILoggerFactory _log;
        public readonly ILogger _log;


        public ScheduleJobBusiness()
        {
            string strConnectionString = GetConnetionDefault();
            //_context = new DbContextClass(strConnectionString);
            //_log = ConfigurationManagerHelpers.Logger;
            //_log = CreateLogger();

        }
        public void ClearRecuringJob()
        {
            BackgroundJobHelper.ClearRecuringJob(ScheduleJobInfo.DailyReminSendEmailSign);
        }

        public void RecuringSendEmailSignInvoice(int minute = 2)
        {
            //var logger = _log.CreateLogger("mylogger");
            //logger.LogInformation("DUCLV app");
            //logger.LogWarning("Lỗi app");

            //_log.LogInformation("DUCLV app");
            //_log.LogWarning("Lỗi app");
            BackgroundJobHelper.RecurringMinutely(ScheduleJobInfo.DailyReminSendEmailSign, () => SendEmailNoticeSignInvoice(), minute);
        }

        public void SendEmailNoticeSignInvoice()
        {
            //_log.LogInformation("DUCLV app");
            //_log.LogWarning("Lỗi app");
            //var tenantRegisterJobs = GetTenantRegisterJobs();
            //if (tenantRegisterJobs == null) return;
            //foreach (var item in tenantRegisterJobs)
            //{
            //    ProcessRunJob(item);
            //}
        }

        private List<TenantRegisterJob> GetTenantRegisterJobs()
        {
            var tenantRegister = _context.TenantRegisterJob.Where(p => p.TimeRun <= DateTime.Now && p.NumberRun != p.NumberSend).ToList();
            return tenantRegister;
        }
        private static string GetConnetionDefault()
        {
            try
            {
                string config = ConfigurationManagerHelpers.AppSetting[CommonConstants.KeyConnectionString];
                if (string.IsNullOrEmpty(config)) return CommonConstants.ConnectionDefault;
                return config;
            }
            catch
            {
                return CommonConstants.ConnectionDefault;
            }
        }

        private async void ProcessRunJob(TenantRegisterJob tenantRegisterJob)
        {
            if (tenantRegisterJob == null) return;
            var minvoiceService = GetMinvoiceService(tenantRegisterJob);
            var minvoiceRequest = CreateMInvoicRequest(tenantRegisterJob);
            var result = await RecuringSendEmail(minvoiceService, minvoiceRequest);
            if (result.Result && (tenantRegisterJob.NumberSend?? 0) < (tenantRegisterJob.NumberRun ?? 0))
            {
                UpdateNextRun(tenantRegisterJob);
            } else
            {
                UpdateFinish(tenantRegisterJob);
            }
        }
        private void UpdateNextRun(TenantRegisterJob tenantRegisterJob)
        {
            int numberMinuteNextStep = (tenantRegisterJob.MinuteNextStep ?? 30);
            tenantRegisterJob.TimeRun = (tenantRegisterJob.TimeRun ?? DateTime.Now).AddMinutes(numberMinuteNextStep);
            tenantRegisterJob.NumberRun = (tenantRegisterJob.NumberRun ?? 0) + 1;
            _context.TenantRegisterJob.Update(tenantRegisterJob);
        }

        private void UpdateFinish(TenantRegisterJob tenantRegisterJob)
        {
            int numberMinuteNextStep = (tenantRegisterJob.NumberRun ?? 5) * (tenantRegisterJob.MinuteNextStep ?? 30) * -1;
            tenantRegisterJob.TimeRun = (tenantRegisterJob.TimeRun ?? DateTime.Now).AddDays(1).AddMinutes(numberMinuteNextStep);
            tenantRegisterJob.NumberRun = 0;
            _context.TenantRegisterJob.Update(tenantRegisterJob);
        }

        private async Task<MInvoiceReponse> RecuringSendEmail(IMinvoiceService minvoiceService, MInvoiceRequest mInvoiceRequest)
        {
            var reponse = await minvoiceService.RecurringSendEmail<MInvoiceReponse>(mInvoiceRequest);
            return reponse;
        }

        private static IMinvoiceService GetMinvoiceService(TenantRegisterJob tenantRegisterJob) 
        {
            return tenantRegisterJob.Version switch
            {
                MinvoiceVersion.InvoiceVersion1 => GetServiceInvoiceVersionOne(tenantRegisterJob),
                MinvoiceVersion.InvoiceVersion2 => GetServiceInvoiceVersionTwo(tenantRegisterJob),
                _ => GetServiceInvoiceVersionTwo(tenantRegisterJob),
            };
        }

        private static MinvoiceServiceV2 GetServiceInvoiceVersionOne(TenantRegisterJob tenantRegisterJob)
        {
            var minvoiceService = new MinvoiceServiceV2(tenantRegisterJob?.UrlAPI, tenantRegisterJob?.TokenKey, tenantRegisterJob?.Token);
            return minvoiceService;
        }

        private static MinvoiceServiceV2 GetServiceInvoiceVersionTwo(TenantRegisterJob tenantRegisterJob)
        {
            var minvoiceService = new MinvoiceServiceV2(tenantRegisterJob.UrlAPI, tenantRegisterJob.TokenKey, tenantRegisterJob.Token);
            return minvoiceService;
        }

        private static MInvoiceRequest CreateMInvoicRequest(TenantRegisterJob tenantRegisterJob)
        {
            return new MInvoiceRequest()
            {
                TaxCode = tenantRegisterJob?.TaxCode,
            };
        }

        //private static ILogger CreateLogger()
        //{
        //    var serviceProvider = new ServiceCollection()
        //                .AddLogging()
        //                .BuildServiceProvider();
        //    return serviceProvider.GetRequiredService<ILogger<ScheduleJobBusiness>>();
        //}
    }
}
