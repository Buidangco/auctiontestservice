﻿using AuctionInitService.BL;
using AuctionInitService.BL.Interface;
using AuctionInitService.Model;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.Options;

namespace AuctionInitService.ServiceExtension
{
    public static class ServiceExtension
    {
        public static IServiceCollection AddIServicesScoped(this IServiceCollection services, WebApplicationBuilder builder)
        {
            var redisOptionRedisAuth = new RedisOption();
            builder.Configuration.GetSection("RedisAuth").Bind(redisOptionRedisAuth);
            services.AddSingleton<IAuthDistributedCache, AuthDistributedCache>(p =>
            {
                var options = new RedisCacheOptions
                {
                    InstanceName = redisOptionRedisAuth.InstanceName,
                    ConfigurationOptions = new StackExchange.Redis.ConfigurationOptions
                    {
                        Password = redisOptionRedisAuth.Password,
                        ConnectRetry = redisOptionRedisAuth.ConnectRetry,
                        AbortOnConnectFail = redisOptionRedisAuth.AbortOnConnectFail,
                        ConnectTimeout = redisOptionRedisAuth.ConnectTimeout,
                        SyncTimeout = redisOptionRedisAuth.SyncTimeout,
                        DefaultDatabase = redisOptionRedisAuth.DefaultDatabase,
                    }
                };
                if (redisOptionRedisAuth.EndPoints != null)
                {
                    foreach (var item in redisOptionRedisAuth.EndPoints)
                    {
                        options.ConfigurationOptions.EndPoints.Add(item);
                    }
                }
                //options.Value.Configuration = ...  set you server IP, etc
                return new AuthDistributedCache(Options.Create(options));
            });

            var redisOptionBidHistory = new RedisOption();
            builder.Configuration.GetSection("RedisBidHistory").Bind(redisOptionBidHistory);
            services.AddSingleton<IRedisCacheService, RedisCacheService>(p =>
            {
                var options = new RedisCacheOptions
                {
                    InstanceName = redisOptionBidHistory.InstanceName,
                    ConfigurationOptions = new StackExchange.Redis.ConfigurationOptions
                    {
                        Password = redisOptionBidHistory.Password,
                        ConnectRetry = redisOptionBidHistory.ConnectRetry,
                        AbortOnConnectFail = redisOptionBidHistory.AbortOnConnectFail,
                        ConnectTimeout = redisOptionBidHistory.ConnectTimeout,
                        SyncTimeout = redisOptionBidHistory.SyncTimeout,
                        DefaultDatabase = redisOptionBidHistory.DefaultDatabase,
                    }
                };
                if (redisOptionBidHistory.EndPoints != null)
                {
                    foreach (var item in redisOptionBidHistory.EndPoints)
                    {
                        options.ConfigurationOptions.EndPoints.Add(item);
                    }
                }
                //options.Value.Configuration = ...  set you server IP, etc
                return new RedisCacheService(Options.Create(options));
            });

            var redisOptionRedisAuction = new RedisOption();
            builder.Configuration.GetSection("RedisAuction").Bind(redisOptionRedisAuction);
            services.AddSingleton<IActionDistributedCache, ActionDistributedCache>(p =>
            {
                var options = new RedisCacheOptions
                {
                    InstanceName = redisOptionRedisAuction.InstanceName,
                    ConfigurationOptions = new StackExchange.Redis.ConfigurationOptions
                    {
                        Password = redisOptionRedisAuction.Password,
                        ConnectRetry = redisOptionRedisAuction.ConnectRetry,
                        AbortOnConnectFail = redisOptionRedisAuction.AbortOnConnectFail,
                        ConnectTimeout = redisOptionRedisAuction.ConnectTimeout,
                        SyncTimeout = redisOptionRedisAuction.SyncTimeout,
                        DefaultDatabase = redisOptionRedisAuction.DefaultDatabase,
                    }
                };
                if (redisOptionRedisAuction.EndPoints != null)
                {
                    foreach (var item in redisOptionRedisAuction.EndPoints)
                    {
                        options.ConfigurationOptions.EndPoints.Add(item);
                    }
                }
                //options.Value.Configuration = ...  set you server IP, etc
                return new ActionDistributedCache(Options.Create(options));
            });
            services.AddSingleton<IUserService, UserService>();
            services.AddSingleton<IBidService, BidService>();
            services.AddSingleton<IActionlogService, ActionlogService>();
            services.AddSingleton<IRoomService, RoomService>();
            return services;
        }
    }
}
